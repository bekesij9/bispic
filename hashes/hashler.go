/*



*/

package hashes

import (
	"os"
	"fmt"
	_ "time"
	_ "flag"
	"crypto/sha256"
	"crypto/md5"
	// log "github.com/sirupsen/logrus"
    "github.com/romana/rlog"
    // "log" // so we can do "log.Panic": we cannot, use "panic" alone...
	"io"
	// "strconv"
	_ "os/exec"
	_ "bytes"
	"path"
	_"runtime"
	_ "path/filepath"

	 "github.com/jinzhu/gorm"
  	_ "github.com/jinzhu/gorm/dialects/sqlite"
  	"sync"
  	_ "sort"
    "github.com/dixonwille/skywalker"
    sha256s "github.com/minio/sha256-simd"

   )


var (
	Root string
	Dirnames = []string{"sha256", "md5"}
	Configfile string
    Action string = "create" // what shall ShaMD5Worker do
    NoExt bool = false
    Simd bool = false
    Workers int = 20
    NoLinks = false
)

type ShaInfo struct {
	gorm.Model
	Filepath string
	Sha256 string
	Size int64
	StorageUnitID int32
} 

type StorageUnit struct {
	gorm.Model
	Name string
	Main_ip string
	Replicas string
}


type ShaWorker struct {
    *sync.Mutex
    found []ShaInfo
}

type ShaMD5Worker struct {
	*sync.Mutex
	count uint
    found []string
    visited uint
}

type ShaSlimWorker struct {
    *sync.Mutex
    count uint
    found []string
    sizeTotal int64
    visited uint
}

func (shaw *ShaWorker) Work(srcpath string) {
    //This is where the necessary work should be done.
    //This will get concurrently so make sure it is thread safe if you need info across threads.
    shaw.Lock()
    defer shaw.Unlock()
   if NoExt == true {
        // return if srcpath is "log"
        if path.Base(srcpath) == "log" { 
            // rlog.(srcpath, "contains .")
            return
        } 
    }    
    sha256 := FileSha256(srcpath)
    var size int64 = 0
    fileinfo, err := os.Stat(srcpath)
	if err != nil {
		rlog.Critical(err)
	} else {
		size = fileinfo.Size()
	}    
    shaw.found = append(shaw.found, 
    				ShaInfo{Filepath: srcpath, Sha256: sha256, Size: size} )
}


// ShaMD5Worker writes a named hardlink into respective directory
func(shaw *ShaMD5Worker) Work(srcpath string) {
	shaw.Lock()
	defer shaw.Unlock()
	var hashes [2]string
	var added bool
    var hashpath string
    var linktarget string
    added = true
    // not necessary: lack of extension only selects dotless filenames!
    if NoExt == true {
        // return if srcpath is "log"
        if path.Base(srcpath) == "log" { 
            // rlog.(srcpath, "contains .")
            return
        } 
    }
    shaw.visited++
    if Action != "reverse" {
	   hashes = FileShaMD5(srcpath)
    }
    switch Action {
        default:
            rlog.Critical("no action chosen")
            // panic("no action available for ShaMD5Worker")
        case "create":
            for i, d := range Dirnames {
                hashpath = path.Join(Root, "..", d, hashes[i][:2], hashes[i][2:4]) 
                err := os.MkdirAll(hashpath, os.FileMode(0777))
                if err != nil {
                    rlog.Critical(err)
                }
                linktarget = path.Join(hashpath, hashes[i])
                // I do not know if it is cheaper to test for file existence or
                // let the error be handled ("file exists")

                err = os.Link(srcpath, linktarget)
                if err != nil {
                    if os.IsExist(err){
                        rlog.Debug("existing hardlink", d, linktarget)                        
                    } else {
                        rlog.Critical(err)
                    }
                }
                err = os.Symlink(srcpath, linktarget + ".lnk")
                if err != nil {
                    if os.IsExist(err){
                        rlog.Debug("existing symlink ", d, linktarget)
                    } else {
                        rlog.Critical(err)
                        // log.Fatal(err)
                    }
                }
            }
            if added == true {
                shaw.count++
            }
        case "update":
            rlog.Warn("not implemented yet: update")
            // only files with no hash entry at all are to be processed, or else
            // errors in checksums will be silently corrected before somebody gets
            // notified!
        case "check":
            // TODO: Implement a plugin or hook to deliver notifications:
            // Mind, the replication boxes are BLACK boxes...
            for i, d := range Dirnames {
                hashpath = path.Join(Root, "..", d, hashes[i][:2], hashes[i][2:4],
                    hashes[i])            
                // file should exist, if not, add to shaw.found
                if _, err := os.Stat(hashpath); os.IsNotExist(err) {
                    rlog.Debug("link does not exist:", hashpath, "for", srcpath)
                    shaw.found = append(shaw.found, d + ":" + srcpath)
                    shaw.count++
                }
            }
        case "reverse":
            // TODO: sift through SHA256 links and check if corresponding nodes are present
            // But we need a different sort of walk algorithm for this
            rlog.Debug("not implemented yet: reverse")

   }
}

// ShaSlimWorker writes a named hardlink into respective directory
func(shaw *ShaSlimWorker) Work(srcpath string) {
    shaw.Lock()
    defer shaw.Unlock()
    var hash string
    var hashpath string
    var linktarget string
    simd := ""
    // not necessary: lack of extension only selects dotless filenames!
    if NoExt == true {
        // return if srcpath is "log"
        if path.Base(srcpath) == "log" { 
            // rlog.(srcpath, "contains .")
            return
        } 
    }
    shaw.visited++
    if Simd {
        hash = FileSha256Simd(srcpath)
        simd = "s"
    } else {
        hash = FileSha256(srcpath)
    }
    hashpath = path.Join(Root, "..", "sha256" + simd, hash[:2], hash[2:4]) 
    err := os.MkdirAll(hashpath, os.FileMode(0777))
    if err != nil {
        rlog.Critical(err)
    }
    if ! NoLinks {
        linktarget = path.Join(hashpath, hash)
        // I do not know if it is cheaper to test for file existence or
        // let the error be handled ("file exists")

        err = os.Link(srcpath, linktarget)
        if err != nil {
            if os.IsExist(err){
                rlog.Debug("existing hardlink sha256",  linktarget)                        
            } else {
                rlog.Critical(err)
            }
        }
        err = os.Symlink(srcpath, linktarget + ".lnk")
        if err != nil {
            if os.IsExist(err){
                rlog.Debug("existing symlink sha256 ", linktarget)
            } else {
                rlog.Critical(err)
                // log.Fatal(err)
            }
        }
    }
    fileInfo, err := os.Stat(srcpath)
    if err != nil {
        rlog.Debug("hrmpf")
    }
    shaw.sizeTotal += fileInfo.Size()
    shaw.count++
}


// FileShaMD5 returns the sha256 hash for a given filepath
// to be called by a goroutine
func FileShaMD5(filepath string) [2]string {
	f, err := os.Open(filepath)
	if err != nil {
		rlog.Critical(err)
	}
	defer f.Close()

	s := sha256.New()
	m := md5.New()
	if _, err := io.Copy(s, f); err != nil {
		rlog.Critical(err)
	}
	f.Seek(0,0)
	if _, err := io.Copy(m, f); err != nil {
		rlog.Critical(err)
	}
	// fmt.Println(h.Sum(nil))
	return [2]string{fmt.Sprintf("%x", s.Sum(nil)), 
		fmt.Sprintf("%x", m.Sum(nil))}
}

// FileSha256 returns the sha256 hash for a given filepath
// to be called by a goroutine
func FileSha256(filepath string) string {
	f, err := os.Open(filepath)
	if err != nil {
		rlog.Critical(err)
	}
	defer f.Close()

	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		rlog.Critical(err)
	}
	// fmt.Println(h.Sum(nil))
	return string(fmt.Sprintf("%x", h.Sum(nil)))
}

// FileSha256Simd returns the sha256 hash for a given filepath
// to be called by a goroutine
func FileSha256Simd(filepath string) string {
    f, err := os.Open(filepath)
    if err != nil {
        rlog.Critical(err)
    }
    defer f.Close()

    h := sha256s.New()
    if _, err := io.Copy(h, f); err != nil {
        rlog.Critical(err)
    }
    // fmt.Println(h.Sum(nil))
    return string(fmt.Sprintf("%x", h.Sum(nil)))
}



// ProcessAssetsDB writes hash values into sqlite db for later
// usage
func ProcessAssetsDB() uint {
	cfg, hostname := GetHostConfig()
    dbname := cfg.GetString("common.dbname")
	dbdir := cfg.GetString(hostname + ".dbdir")
	testdir := cfg.GetString(hostname + ".testdir")
	db, err := gorm.Open("sqlite3", path.Join(dbdir, 
		dbname +  hostname + ".sqlite3"))

	if err != nil {
    	panic("found to connect database")
  	}
  	defer db.Close()
	shaw := new(ShaWorker)
    shaw.Mutex = new(sync.Mutex)
    NoExt = cfg.GetBool(hostname + ".noExt")
    rlog.Info("NoExt:", NoExt)    
    // rlog.Info("starting in: ", Root)
    // ======
    // root is the root directory of the data
    /*
    sw := skywalker.New(Root, shaw)
    sw.ExtListType = skywalker.LTWhitelist
    extList := make([]string, 20)
    extListCfg, err := cfg.List(hostname + ".extensions")
    if err != nil {
        rlog.Critical(err)
    }
    NoExt, err = cfg.Bool(hostname + ".noExt")
    if err != nil {
        rlog.Debug("no noExt in cfg")
    }
    rlog.Info("NoExt:", NoExt)
    for i, item := range extListCfg {
        extList[i] = item.(string)
    }
    sw.ExtList = extList
    */
    // =====
	// db.CreateTable(&ShaInfo{})
	// Migrate the schema
  	db.AutoMigrate(&ShaInfo{})
  	db.AutoMigrate(&StorageUnit{})
  	// root is the root directory of the data that was stood up above
    sw := skywalker.New(testdir, shaw)
    if hostname == "phoebe" {
    	sw.DirListType = skywalker.LTWhitelist
    	sw.DirList = []string{"/2018-01-20", "/2018-02-08"}
	}
    sw.ExtListType = skywalker.LTWhitelist
    extList := make([]string, 20)
    extListCfg := cfg.GetStringSlice(hostname + ".extensions") 
    for i, item := range extListCfg {
    	extList[i] = item
    }
    sw.ExtList = extList
    walk_err := sw.Walk()
    if walk_err != nil {
        rlog.Error(walk_err)
        return 0
    }
    // sort.Sort(sort.StringSlice(shaw.found))
    rlog.Info(len(shaw.found), "assets found, updating db...")
    for _, f := range shaw.found {
        // fmt.Println(f[0]) // strings.Replace(f, sw.Root, "", 1))

        // var si = ShaInfo{Filepath: f.Filepath, Sha256: f.Sha256, Size: f.Size}
        db.Create(&f)
    }
    return uint(len(shaw.found))
}

// ProcessAssetsWriteFS writes sha256 and md5 hardlinks into resp. directories
// together with a symlink to the original file
func ProcessAssetsWriteFS() (uint, uint, []string) {
	cfg, hostname := GetHostConfig()
	shaw := new(ShaMD5Worker)
    shaw.Mutex = new(sync.Mutex)
    // rlog.Info("starting in: ", Root)

  	// root is the root directory of the data
    sw := skywalker.New(Root, shaw)
    sw.ExtListType = skywalker.LTWhitelist
    extList := make([]string, 20)
    extListCfg  := cfg.GetStringSlice(hostname + ".extensions")
    NoExt= cfg.GetBool(hostname + ".noExt")
    rlog.Info("NoExt:", NoExt)
    for i, item := range extListCfg {
    	extList[i] = item
    }
    sw.ExtList = extList
    walk_err := sw.Walk()
    if walk_err != nil {
        rlog.Error(walk_err)
        return 0, 0, []string{}
    }
    // sort.Sort(sort.StringSlice(shaw.found))
    // rlog.Info(shaw.found, " assets found and written")
    return shaw.count, shaw.visited, shaw.found
}

// ProcessAssetsWriteFSSlim writes sha256 hardlinks into resp. directories
// together with a symlink to the original file
// Slim version, perhaps faster: no switch, tuned down to essentials
func ProcessAssetsWriteFSSlim() (uint, uint, []string, int64) {
    cfg, hostname := GetHostConfig()
    shaw := new(ShaSlimWorker)
    shaw.Mutex = new(sync.Mutex)
    // rlog.Info("starting in: ", Root)

    // root is the root directory of the data
    sw := skywalker.New(Root, shaw)
    sw.ExtListType = skywalker.LTWhitelist
    sw.NumWorkers = Workers
    extList := make([]string, 20)
    extListCfg := cfg.GetStringSlice(hostname + ".extensions")
    NoExt = cfg.GetBool(hostname + ".noExt")
    rlog.Debug("NoExt:", NoExt)
    if NoExt {
        extList[0] = ".improbable"
    } else {
        for i, item := range extListCfg {
            extList[i] = item
        }
    }
    sw.ExtList = extList
    walk_err := sw.Walk()
    if walk_err != nil {
        rlog.Error(walk_err)
        return 0, 0, []string{}, 0
    }
    // sort.Sort(sort.StringSlice(shaw.found))
    // rlog.Info(shaw.found, " assets found and written")
    return shaw.count, shaw.visited, shaw.found, shaw.sizeTotal
}


// ProcessAssetsCheckLink checks sha256 hardlinks: does its link
// still exist (but not if valid!)
func ProcessAssetsCheckLink() (uint, uint, []string) {
    // cfg, hostname := GetHostConfig()
    shaw := new(ShaMD5Worker)
    shaw.Mutex = new(sync.Mutex)
    // rlog.Info("starting in: ", Root)

    // root is the root directory of the data
    root := path.Join(Root, "..", Dirnames[0])
    sw := skywalker.New(root, shaw)
    sw.ExtListType = skywalker.LTWhitelist
    extList := []string{".lnk"}
    sw.ExtList = extList
    walk_err := sw.Walk()
    if walk_err != nil {
        rlog.Error(walk_err)
        return 0, 0, []string{}
    }
    return shaw.count, shaw.visited, shaw.found
}
