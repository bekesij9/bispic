/*

	Testing a few aspects of hashler...

*/

package hashes

import (
	"os"
	"fmt"
	"io"
	"bufio"
    // sha256s "github.com/minio/sha256-simd"
    "crypto/sha256"
	"crypto/md5"
    "github.com/romana/rlog"
	)


// TestShaMD5 tries to examine components for 
// re-use of read filecontent (to feed into other hash constructors)
// 
func TestShaMD5(filepath string) [2]string {
	f, err := os.Open(filepath)
	if err != nil {
		rlog.Critical(err)
	}
	defer f.Close()

	bufsize := 16 * 1024 * 1024 // 16MB
	s_buf := sha256.New()
	su_buf := sha256.New()
	m_buf := md5.New()
	bufferedReader := bufio.NewReaderSize(f, bufsize)
	rlog.Info("Buffered: ", bufferedReader.Buffered())
	bigbuf := make([]byte, bufsize)
	bytes_read := 1
	for bytes_read > 0 {
		bytes_read_r, err := bufferedReader.Read(bigbuf) 
		if err != nil {
			if err == io.EOF {
				rlog.Debug("done reading buffer", bytes_read_r)
				break
			} else {
				rlog.Critical(err)
			}
		}
		bytes_read = bytes_read_r
		s_buf.Write(bigbuf[:bytes_read_r])
		m_buf.Write(bigbuf[:bytes_read_r])
		rlog.Debug("writing chunk with length", bytes_read_r, len(bigbuf))
	}
	f.Seek(0,0)
	if _, err := io.Copy(su_buf, f); err != nil {
		rlog.Critical(err)
	}
	rlog.Info(fmt.Sprintf("Buffered Sha256  : %x", s_buf.Sum(nil)))
	rlog.Info(fmt.Sprintf("UnBuffered Sha256: %x", su_buf.Sum(nil)))
	rlog.Info(fmt.Sprintf("Buffered MD5     : %x", m_buf.Sum(nil)))
	s := sha256.New()
	m := md5.New()

	for _, chunk := range([]string{"hello world\n", "go and get me\n"}) {
		s.Write([]byte(chunk))
		m.Write([]byte(chunk))
	}
	s2 := sha256.New()
	s2.Write([]byte("hello world\ngo and get me\n"))
	rlog.Info(fmt.Sprintf("%x", s2.Sum(nil)))


	return [2]string{fmt.Sprintf("%x", s.Sum(nil)), 
		fmt.Sprintf("%x", m.Sum(nil) )} 

}

