/*

Utilities


*/

package hashes

import (
	"os"
	"bufio"
	"io"
	"fmt"
	"crypto/sha256"
	"crypto/md5"
	sha256s "github.com/minio/sha256-simd"
	"github.com/romana/rlog"
	"github.com/olebedev/config"
    "github.com/spf13/viper"
    "strings"
)

var Md5 bool = false


type BenchmarkInfo struct {
	Description string
	Iterations int
    Durations []float64
    UnitEntries []float64
    Unit string
    Filesize_total uint64
}

// FileHashes returns sha256 and, if set, md5 hash for a 
// given filepath as well as the filesize; to be called 
// by a goroutine
func FileHashes(filepath string) ([2]string, int64) {
    f, err := os.Open(filepath)
    if err != nil {
        rlog.Critical(err)
    }
    defer f.Close()
    bufsize := 16 * 1024 * 1024 // 16MB
    // now size the file:
    fileInfo, err := os.Stat(filepath)
    if err != nil {
        rlog.Debug(err)
    }
    size := fileInfo.Size()
    m_buf := md5.New()
    s_buf := sha256.New()
    // now switch with regard to filesize:
    if (size > 1024 * 1024 * 16) {
    	s_buf = sha256s.New()
    }
	bufferedReader := bufio.NewReaderSize(f, bufsize)
	bigbuf := make([]byte, bufsize)
	bytes_read := 1
	for bytes_read > 0 {
		bytes_read_r, err := bufferedReader.Read(bigbuf) 
		if err != nil {
			if err == io.EOF {
				rlog.Debug("done reading buffer", bytes_read_r)
				break
			} else {
				rlog.Critical(err)
			}
		}
		bytes_read = bytes_read_r
		s_buf.Write(bigbuf[:bytes_read_r])
		if (Md5) {
			m_buf.Write(bigbuf[:bytes_read_r])
		}
	}
	if Md5 {
		return [2]string{fmt.Sprintf("%x", s_buf.Sum(nil)), 
			fmt.Sprintf("%x", m_buf.Sum(nil) )}, size
	} else {
		return [2]string{fmt.Sprintf("%x", s_buf.Sum(nil)), 
			""}, size
	}
}

// GetHostConfig returns the config Map for the current host
func GetHostConfig() (*viper.Viper, string)  {
	hostname, err := os.Hostname()
	if err != nil {
		rlog.Critical(err)
	}
	hostname = strings.Split(hostname, ".")[0]
    // _, filename, _, _ := runtime.Caller(1)
	cfg := viper.Sub(hostname)
    // cfg, err := viper.GetStringMapString(hostname)
	if cfg == nil {
    	rlog.Critical("No config for ", hostname)
    }
    return cfg, hostname
}

// GetHostConfigOld returns the config section for the current host
func GetHostConfigOld() (*config.Config, string)  {
    hostname, err := os.Hostname()
    if err != nil {
        rlog.Critical(err)
    }
    hostname = strings.Split(hostname, ".")[0]
    // _, filename, _, _ := runtime.Caller(1)
    cfg, err := config.ParseYamlFile(Configfile)
    // cfg, err := viper.GetStringMapString(hostname)
    if err != nil {
        rlog.Critical(err)
    }
    return cfg, hostname
}

// MbPerSec returns MegaBytes per Second value (for metrics)
func MbPerSec(Bytes int64, Seconds float64) float64 {
	mbps := float64(Bytes) / Seconds / (1024 * 1024)
	rlog.Debug(fmt.Sprintf("     %.2f MBytes per Second", mbps))
	return mbps
}

// SetDebug is called by commands to set the debug level for logging
func SetDebug(debug bool) {
	if debug {
		os.Setenv("RLOG_LOG_LEVEL", "DEBUG")
	    rlog.UpdateEnv()
	}
}

// WriteBench returns benchmark info 
func WriteBench(info *BenchmarkInfo) string {
	total := 0.0
	total_unit := 0.0
	output := "Benchmark: " + info.Description + "\n"
	for i, d := range(info.Durations) {
		total += d
		total_unit += info.UnitEntries[i]
		output += fmt.Sprintf("%20.2f  (%4.2f %s)\n", d, info.UnitEntries[i], info.Unit)
	}
	avg_unit := total_unit / float64(len(info.Durations))
	output += fmt.Sprintf("=========================================\n")
	output += fmt.Sprintf("%20.2f  (%4.2f %s avg)\n", total, avg_unit, info.Unit)
	return output
}

