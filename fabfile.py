# -*- coding: utf8 -*-
#
# Use this to deploy this on all different servers for testing
#
# First, we use this with virdam and the assets from our test runs
#
from __future__ import absolute_import, print_function
import datetime
import os
import logging


from fabric.api import local, run, env, cd, lcd, put, get, settings, warn_only
from fabric.colors import red, green, cyan, magenta
"""
logger = logging.getLogger(__name__)
logging.basicConfig(filename="fabfile.log",
                    level=logging.INFO,
                    format='%(levelname)s [%(asctime)s]: %(message)s',
                    datefmt='%Y-%m-%d %H:%M')
"""
env.hosts = ['root@virdam', "jb@rex001.phaidra.org"]
env.hosts = ['root@virdam.univie.ac.at']


WORKDIR = {
            'virdam': '/root/work/go/src/ghe.phaidra.org/bekesij9', 
            'rex001': '/home/jb/go/src/ghe.phaidra.org/bekesij9'
        }
LOCAL = os.path.dirname(os.path.realpath(__file__))
PKGNAME = os.path.split(LOCAL)[-1]
NOW = datetime.datetime.now()
NOW_FMT = NOW.strftime("%Y%m%d-%H%M")


def _cur_host():
    "return current hostname"
    return env.host_string.split('@')[-1].split('.')[0]


def go_run(prog='main.go'):
    "run a go program (`go_run:programname`)"
    pkg = ""
    if prog == 'main.go':
        pkg = PKGNAME + '/cmd/bispic'
    with cd(WORKDIR[_cur_host()] + '/' + pkg):
        run("go run {}".format(prog))

def go_install(prog='main.go', add_rev=False):
    "run a go program (`go_run:programname`)"
    pkg =""
    rev = ""
    options = ""
    if add_rev:
        rev = local("git rev-parse --short HEAD", capture=True)
        options = """-ldflags="-X ghe.phaidra.org/bekesij9/bispic/cmd/root.GitVersion={}" """.format(
            rev)
        # we do not use the flags, but `govvv` instead
        options = "" # -pkg ghe.phaidra.org/bekesij9/bispic/cmd"
    if prog == 'main.go':
        # pkg = PKGNAME + '/cmd/bispic'
        pkg = PKGNAME # + '/cmd/bispic'
    with cd(WORKDIR[_cur_host()] + '/' + pkg):
        run("govvv build {} {}; govvv install".format(options, prog))
    

def deploy(prog='main.go'):
    "deploy the installed program to deploy area"
    gitupd(stashfirst=True)
    go_install(prog=prog, add_rev=True)
    workroot = WORKDIR[_cur_host()][:WORKDIR[
        _cur_host()].find('go/')+3]
    print(workroot)
    with cd(workroot):
        run("mkdir -p ./deploy")
        run("cp ./bin/{} ./deploy/{}.{}".format("bispic", "bispic",
            NOW_FMT))
    with cd(workroot + '/bin'):
        run("bispic --version")       

def gitupd(stashfirst=False):
    "update git on virdam"
    with cd(WORKDIR[_cur_host()]+ '/' + PKGNAME):
        local("git push")
        if stashfirst:
            run("git stash")
        run("git pull")

def go_get(source=None):
    "load a package"
    if not source:
        print("no source!")
    with cd(WORKDIR[_cur_host()]+ '/' + PKGNAME):
        run('go get -u {}'.format(source))

