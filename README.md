# Bit Stream Preservation Infrastructure Components

## Components (Roadmap)

1. Adding hashes
2. Checking hashes
3. Notification
4. Dashboard / UI
5. Database Plugin (Mongodb)
6. Configuration


## Bits

### Local Checks

Should local checks have precedence over networked ones? They surely are faster, and can be
telling as well as networked ones.


### Networked Checks

Which data should be sent over the network? How to assure its authenticity and completeness/ 
correctness? Public key crypto, I presume.


## Tools:

* Using `go get github.com/ahmetb/Govvv` for version, revision and date output.
* Using `fab deploy` for deploying `bispic` file to `virdam.univie.ac.at:/root/work/go/bin`


## Benchmarks

### Iteration 0

Only retrieve hashes and store results inside sqlite db

= 1000 assets / 240 secs

(But how could `ingesting 2.000 assets took 47 secs. (first version, writing into sqlite)` be? 
Was a result in `ghe.phaidra.org/explore`. We might have to try and re-run this version to be sure)

### Iteration 1

Ingesting 7.800 assets took 48 mins. (hardlink sha256 and md5 and symlink), so
this approach is 15 times slower than sqlite-storing; should we use a queue  instead?

= 1.000 assets / 369 secs

### Iteration 2

1.000 assets / 312 secs (hardlink sha256 and symlink)


### Iteration 3

Only use one hash algorithm and a slim worker:

1.000 assets / 240 secs

### Comparison with a python script (no concurrency)

A quickly concocted `hashler.py` script (no concurrency, `sha256` only) took 550 secs 
for 1.056 assets, about twice as long as the `Go` version. Also, the hashes are different 
(implementation failure, obviously, but on which side?)

### Iteration 4

A closer look revealed that our test data were biased: 1.000 files comprised 30GB, which is 
tenfold the size to expect in real operation.

So  we set up a more lifelike test environment: about 10.000 files contain 30GB.

And we found a way better metric: **MBytes/second**, so we can do a better estimation of time needed
for processing.

## Benchmarking

Was a bit futile, since it gave a slow first and then fast subsequent results, due to the fact 
that the os caches all file access (as far as its RAM allows). So there are no quick
estimates to gain -- we must use really BIG files to test this (or files not accessed for a longer
period of time).

Well. 




