/*
    main cli app
*/
package main

import (
	"fmt"
	_"runtime"
	"os"
    "time"
	// _ "github.com/olebedev/config"
	//  _ "github.com/dixonwille/skywalker"
	// _ "sync"
    "flag"
    "path"
    "path/filepath"
    "strings"
    // "github.com/sirupsen/logrus"
    "github.com/romana/rlog"
    "ghe.phaidra.org/bekesij9/bispic"
    
)

var (
   root string
   usage = "Usage: ./exp03 <rootdirectory>\n" 
   dirnames string
   dirnamesPrevious = []string{"sha256", "md5"}
   workers int
   doCheck bool 
   doAdd bool
   doSimd bool
   doUpdate bool 
   doCheckReverse bool
   doDb bool
   doSlim bool
   debug bool
   doVersion bool
   Version string
   GitCommit string
   BuildDate string
   Date string
)

func init() {
    flag.StringVar(&root, "root", ".", "root directory to begin processing with")
    flag.StringVar(&dirnames, "dirnames", "sha256,md5", "comma separated names of directories to store hashes in", )
    flag.BoolVar(&doCheck, "check", false, "check directory")
    flag.BoolVar(&doAdd, "add", false, "add hashes for directory")
    flag.BoolVar(&doSimd, "enh", false, "use enhanced sha256")
    flag.BoolVar(&doSlim, "add-slim", false, "add hashes for directory (slim and fast)")
    flag.BoolVar(&doUpdate, "update", false, "update hashes for directory (= add new ones)")
    flag.BoolVar(&doCheckReverse, "reverse", false, "check hash directories for missing nodes")
    flag.BoolVar(&doDb, "db", false, "insert hashes into db")
    flag.IntVar(&workers, "workers", 20, "number of concurrent workers")
    flag.BoolVar(&debug, "d", false, "write debug to log")
    flag.BoolVar(&doVersion, "version", false, "show version")
    os.Setenv("RLOG_LOG_FILE", "bispic.log")
    rlog.UpdateEnv()
}


func main() {
    flag.Parse()
    if (debug) {
        os.Setenv("RLOG_LOG_LEVEL", "DEBUG")
        rlog.UpdateEnv()
    }
    var total, visited uint
    var sizeTotal int64
    var failed []string
    if doVersion {
        fmt.Printf("Version %s, Rev %s, UTC %s\n", Version, GitCommit, BuildDate)
        return
    }    
    action := "create"
    cliroot, err := filepath.Abs(root)
    if err != nil {
        rlog.Critical(err)
    }
    // transfer cli flags to hashler:
    bispic.Dirnames = strings.Split(dirnames, ",")
    bispic.Configfile = "bispic.yml"
    bispic.Workers = workers
    cfg, hostname := bispic.GetHostConfig()
    newroot, err := cfg.String(hostname + ".testdir")
    if err != nil {
        rlog.Debug("no root value from config")
        newroot = cliroot
    } 
    if root != "." {
        newroot = cliroot
    }
    noLinks, err := cfg.Bool(hostname + ".noLinks")
    if err != nil {
        rlog.Debug("no noLinks value from config")
        noLinks = false
    }
    removeHashes, err := cfg.Bool(hostname + ".removeHashes")
    if err != nil {
        rlog.Debug("no removeHashes value from config")
        removeHashes = false
    }
    rlog.Info("noLinks:", noLinks, ", removeHashes:", removeHashes)
    bispic.NoLinks = noLinks
    bispic.Root = newroot
    if removeHashes {
        rlog.Info("removing hash directories")
        for _, hash := range []string{"sha256", "sha256s"} {
            os.RemoveAll(path.Join(newroot, "..", hash))
        }
    }
    rlog.Info("starting", GitCommit, " at:", bispic.Root)
    if noLinks {
        rlog.Info("NOT creating hard- and symlinks")
    }
    t0 := time.Now()
    if doAdd {
        total, visited, _ = bispic.ProcessAssetsWriteFS()
        action = "creat"
    }
    if doSimd {
        rlog.Info("Trying enhanced sha256 algorithm")
        bispic.Simd = true
    }
    if doSlim {
        // remove
        total, visited, _, sizeTotal = bispic.ProcessAssetsWriteFSSlim()
        avg := float64(sizeTotal) / float64(total) / (1024.0 * 1024.0)
        avg = float64(int64(avg * 10.0)) / 10.0
        rlog.Info(fmt.Sprintf("%v files with average size of %v MB", total, avg))
        action = "slim creat"
    }    
    if doCheck {
        // rlog.Warn("Check not implemented yet")
        action = "check"
        bispic.Action = action
        total, visited, failed = bispic.ProcessAssetsWriteFS()
        for _, f := range failed {
            rlog.Critical("failed at check:", f)
        }
    }
    if doUpdate {
        rlog.Warn("Update not implemented yet")
        action = "update"
        bispic.Action = action
        // do the update
        action = "updat" // for output
    }
    if doDb {
        action = "database insert"
        total = bispic.ProcessAssetsDB()
    }

    if doCheckReverse {
        rlog.Warn("Update not implemented yet")
    }
    t1 := time.Now()
    if doSlim {
        sseconds := t1.Sub(t0).Seconds()
        // fmt.Println("seconds: ", sseconds)
        mbps := float64(sizeTotal) / float64(sseconds) / (1024 * 1024)
        rlog.Info(fmt.Sprintf("=> MBytes per Second: %.2f", mbps))
    }
    rlog.Info(fmt.Sprintf("...%ving %v objects (%v visited assets) took %v to run.", action, 
        total, visited, t1.Sub(t0)))
}


// InitDirs creates md5 and sha256 directories
func InitDirs(root string) {
    for _, d := range dirnamesPrevious {
        fmt.Println(d)
        err := os.MkdirAll(path.Join(root, "..", d), os.FileMode(0777))
        if err != nil {
            rlog.Critical(err)
        }
    }
}