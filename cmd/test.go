// Copyright © 2018 Janos Bekesi <janos.bekesi@univie.ac.at>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"time"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
    "ghe.phaidra.org/bekesij9/bispic/hashes"	
    "github.com/romana/rlog"
)

var (
	DoFilehashes bool
	DoBenchmark bool
	iterations int
    total, visited uint
    sizeTotal, iterSizeTotal int64	
	)


func init() {
	rootCmd.AddCommand(testCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// testCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// testCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	testCmd.Flags().IntVar(&iterations, "iterations", 10, "iterations for benchmark")
	testCmd.Flags().BoolVarP(&DoFilehashes, "filehashes", "", false, "test the fileHashes function")
	testCmd.Flags().BoolVarP(&DoBenchmark, "benchmark", "", false, "do a benchmark and write it to bench")
}

// testCmd represents the test command
var testCmd = &cobra.Command{
	Use:   "test",
	Short: "This is the testing ground for new functionalities",
	Long: `Not the unittest or gotest area, just for trying out new functions and
concepts.`,
	Run: func(cmd *cobra.Command, args []string) {
		hashes.SetDebug(Debug)
		// rlog.Info("Running ok")
		cfg, _ := hashes.GetHostConfig()
		// hashes.Setup()
		// testfile := "/Users/janos/Downloads/TestExport.zip" // 82MB
		testfile := cfg.GetString("testfile") //"/Users/janos/Downloads/dbeaver-ce-5.0.0-macos.dmg"
		hashes.Root = cfg.GetString("testdir")
		hashes.NoExt = cfg.GetBool("noExt")
		hashes.NoLinks = cfg.GetBool("noLinks")
		// hashes.RemoveHashes = cfg.GetBool("removeHashes")
		if DoFilehashes {
			tstart := time.Now()
			res_hashes, size := hashes.FileHashes(testfile)
			seconds := time.Now().Sub(tstart).Seconds()
			rlog.Info(fmt.Sprintf("this was: %.2f MB/s", hashes.MbPerSec(size, seconds)))
			rlog.Info(res_hashes[0], res_hashes[1], size)
			return
		}
		if DoBenchmark { // ten times the same

			var info = hashes.BenchmarkInfo{	Description : "WriteFS Slim",
				Iterations : iterations,
				Unit : "MBpS",}
				// Durations: make([]float64, iterations),
				// UnitEntries: make([]float64, iterations)}
			iterSizeTotal = 0
			for i := 0; i < iterations; i++ {
				tstart := time.Now()
				total, visited, _, sizeTotal = hashes.ProcessAssetsWriteFSSlim()
				seconds := time.Now().Sub(tstart).Seconds()
        		// avg := float64(sizeTotal) / float64(total) / (1024.0 * 1024.0)
        		mps := hashes.MbPerSec(sizeTotal, seconds)
        		info.Durations = append(info.Durations, seconds)
        		info.UnitEntries = append(info.UnitEntries, mps)
        		iterSizeTotal += sizeTotal      		
			}
			info.Filesize_total = uint64(iterSizeTotal)
			rlog.Info(hashes.WriteBench(&info))
		return
		}
		rlog.Info(hashes.TestShaMD5(testfile))
		rlog.Debug("viper: ", viper.GetString("common.dbname"))
	},
}
