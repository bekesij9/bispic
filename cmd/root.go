// Copyright © 2018 Janos Bekesi <janos.bekesi@univie.ac.at>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"os"
	"time"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
    "github.com/romana/rlog"
)

var (
   cfgFile string
   Debug, Verbose bool
   Version string
   GitCommit string
   BuildDate string
   Date string
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "bispic",
	Version: "N.A",
	Short: "bitstream preservation infrastructure components",
	Long: `Bispic is a CLI application for various aspects of digital and bitstream
preservation and archiving. It can be run as a client or a server, is lightweight 
and robust. ;-) as are all golang programs... allegedly.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
 		
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	const longForm = time.RFC3339
	loc, _ := time.LoadLocation("Europe/Vienna")
	bdate, _ := time.Parse(longForm, BuildDate)
	bdate = bdate.In(loc)
    rootCmd.SetVersionTemplate("Version: " + Version + ", Rev "+ GitCommit + " compiled at " + 
    	bdate.Format(time.RFC3339) + "\n")
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/bispic.yml)")
	rootCmd.PersistentFlags().BoolVarP(&Debug, "debug",  "d", false, "display debug information")
	rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "verbose output")	
    os.Setenv("RLOG_LOG_FILE", "bispic.log")
    rlog.UpdateEnv()	
	// rootCmd.Flags().BoolP(&Version, "version", "v", "N.A.", "print version information")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			rlog.Critical(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".bispic2" (without extension).
		viper.AddConfigPath(home)
		viper.AddConfigPath(".")
		viper.SetConfigType("yaml")
		viper.SetConfigName("bispic")
	}
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		rlog.Debug("Using config file:", viper.ConfigFileUsed())
	}
}
