// Copyright © 2018 Janos Bekesi <janos.bekesi@univie.ac.at>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
    "ghe.phaidra.org/bekesij9/bispic/hashes"	
)

var (
	DoWriteDB bool
)

// addCmd represents the add command
var unidamCmd = &cobra.Command{
	Use:   "unidam",
	Short: "create unidam hashes in sqlite db",
	Long: `For all unidam files on virdam4 (easydb4), write the file hashes into the source db. 
	Afterwards, we can use these values to read hashes via easydb5 api for comparison.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("called 'unidam'")
		hashes.SetDebug(Debug)
	},
}

func init() {
	rootCmd.AddCommand(unidamCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	unidamCmd.PersistentFlags().StringP("database", "", "source.db", "database to use")
	unidamCmd.Flags().BoolVarP(&DoWriteDB, "writedb", "", false, "write to db")
	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// addCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
