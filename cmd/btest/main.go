/*

Exploring and testing things (again)

*/

package main

import (
    "github.com/romana/rlog"
    "ghe.phaidra.org/bekesij9/bispic"
)

func main() {
	rlog.Info("Starting with buffered committing to hashes")
	testfile := "/Users/janos/Downloads/testnumismatikexport.zip" //TestExport.zip" // 82MB
	rlog.Info(bispic.TestShaMD5(testfile))
}