# -*- coding: utf8 -*-
#
"""
This is  a testfile, thrown together quickly, so we can compare or benchmark
go and python.

we just need one or two functions:

a path walker and a reader/writer of hash values

"""
import os
import sys
import hashlib
import argparse
import logging
from pathlib import Path
import datetime

logging.basicConfig(filename='hashler.py.log'.format(__name__),
                    level=logging.DEBUG,
                    format='%(asctime)s [%(levelname)s] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)

class Hashler(object):
    "adding hashes... "
    exclude_extensions = []
    cnt = 0
    totalsize = 0   
    def __init__(self,  *args, **kw):
        """ class init
        """
        # setting attributes...
        self.now = datetime.datetime.now()
        for i in kw.keys():
            setattr(self, i, kw[i])
        self.root = os.path.dirname(os.path.realpath(__file__))

# ############################################## METHODS  #####

    def setup(self):
        self.exclude_extensions = self.options.ext.split(",")

    def test(self):
        pass

    def hash_me(self, srcpath):
        "return a hash "
        sha256 = ''
        md5 = ""
        with open(srcpath, 'rb') as src:
            sha256 = hashlib.sha256(src.read()).hexdigest()
        return (sha256, md5)


    def process_file(self, srcpath):
        "hashing the sourcepath"
        sha256, md5 = self.hash_me(srcpath)
        filesize = os.stat(srcpath).st_size
        self.totalsize += filesize
        linkdir = os.path.join(self.options.workdir, '..', 'sha256', 
                               sha256[:2], sha256[2:4])
        os.makedirs(linkdir, exist_ok=True)
        try:
            os.link(srcpath, linkdir + os.sep + sha256)
            os.symlink(srcpath, linkdir + os.sep + sha256 + '.lnk')
            self.cnt += 1
        except FileExistsError:
            pass


# ############################################## SEQUENCE #####

    def initialize(self):
        """initializing
        """
        print("starting....")
        logger.info("starting")
        self.setup()
        pass

    def run(self):
        """ run applic
        """
        for root, dirs, files in os.walk(self.options.workdir, topdown=True):
            for name in files:
                if (name == 'log' or (self.options.noext and 
                                      name.find(".") > -1)):
                    continue
                self.process_file(root + os.sep + name)
            pass

    def finalize(self):
        """ clean up
        """
        diff = datetime.datetime.now() - self.now
        print("active for {} secs, bye!".format(diff))
        logger.info("processed {} files".format(self.cnt))
        mps = self.totalsize / diff.seconds / (1024.0 * 1024.0)
        logger.info("speed is {} MB/sec".format(mps))
        logger.info("active {} secs for {} files, bye!".format(diff, self.cnt))
        pass

    def main(self):
        """main runner
        """
        self.initialize()
        self.run()
        self.finalize()


# internal functions & classes

_DESCRIPTION = ""

if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(description=_DESCRIPTION)
        group = parser.add_mutually_exclusive_group()
        group.add_argument("-v", "--verbose", action="store_true")
        group.add_argument("-q", "--quiet", action="store_true")
        parser.add_argument("-d", action='store_true', dest='debug',
                            help="display debug information")
        parser.add_argument("-w", "--workdir", action="store",
                            default=("/Users/janos/Code/unidam5/" 
                            "test_imports/Die besten Bilder der NASA/"),
                            help="working dir (root for hashes)")
        parser.add_argument("--hash", action="store", default="sha256",
                            help="hash algorithm")
        parser.add_argument("--ext", action="store", default="mp3",
                            help="exclude_extensions, comma separated")
        parser.add_argument("--noext", action="store_true", default=True,
                            help="exclude ALL extensions")
        # parser.add_argument("x", type=int, help="the base")
        # parser.add_argument("y", type=int, help="the exponent")
        args = parser.parse_args()
        tst = Hashler(debug=args.debug, options=args)
        tst.main()

    except SystemExit:
        pass
    except Exception:
        import traceback
        traceback.print_exc()





